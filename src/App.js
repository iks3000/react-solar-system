import React from "react";

const TogglerSwitch = () => (
  <div className="form-check form-switch d-flex justify-content-end">
    <input
      className="form-check-input my-check shadow-none"
      type="checkbox"
    />
  </div>
)

const Title = ({ style }) => (
  <h1
    style={{ color: "#999", fontSize: "19px" }}>
    Solar System Planets:
  </h1>
)

const Planets = () => (
  <ul className="planets-list">
    <li>Mercury</li>
    <li>Venus</li>
    <li>Earth</li>
    <li>Mars</li>
    <li>Jupiter</li>
    <li>Saturn</li>
    <li>Uranus</li>
    <li>Neptunes</li>
  </ul>
)

function App() {
  return (
    <>
      <TogglerSwitch />
      <Title />
      <Planets />
    </>
  );
}

export default App;
